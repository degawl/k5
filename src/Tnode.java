import java.util.*;

public class Tnode {

   private String name;
   private Tnode firstChild;
   private Tnode nextSibling;

   private Tnode(String n, Tnode d, Tnode r) {
      name = n;
      firstChild = d;
      nextSibling = r;
   }

   private Tnode(String n) {
      name = n;
      firstChild = null;
      nextSibling = null;
   }

   @Override
   public String toString() {
      StringBuffer b = new StringBuffer();
      b.append(name);
      if (nextSibling != null) {
         b.append("(");
         b.append(nextSibling.toString());
         b.append(",");
      }

      if (firstChild != null) {
         b.append(firstChild);
         b.append(")");
      }
      return b.toString();
   }

   public static Tnode buildFromRPN (String pol) { // Viide https://stackoverflow.com/questions/423898/postfix-notation-to-expression-tree
      String[] polAsArray = pol.split("\\s+");
      for (String token : polAsArray) {
         if (!(isNumeric(token) || isOperator(token))) {
            throw new RuntimeException(String.format("Inputted string '%s' is incorrectly formatted and contains '%s', which cannot be in RPN", pol, token));
         }
      }

      Stack<Tnode> stack = new Stack<>();
      for (int i = 0; i < polAsArray.length; i++) {
         if (isOperator(polAsArray[i])) {
            if (operatorIsSwap(polAsArray[i])) {
               if (getStackSize(stack) > 1) {
                  Tnode firstChild = stack.pop();
                  Tnode nextSibling = stack.pop();

                  stack.push(firstChild);
                  stack.push(nextSibling);
               }
            } else if (operatorIsRot(polAsArray[i])) {
               if (getStackSize(stack) > 2) {
                  Tnode firstChild = stack.pop();
                  Tnode nextSibling = stack.pop();
                  Tnode secondSibling = stack.pop();

                  stack.push(nextSibling);
                  stack.push(firstChild);
                  stack.push(secondSibling);
               }
            } else {
               Tnode firstChild = stack.pop();
               Tnode nextSibling = stack.pop();
               stack.push(new Tnode(polAsArray[i], firstChild, nextSibling));
            }
         } else {
            stack.push(new Tnode(polAsArray[i]));
         }
      }
      if (stack.size() != 1) {
         throw new RuntimeException(String.format("Inputted string '%s' has too many arguments.", pol));
      }
      return stack.peek();
   }

   static boolean isOperator(String o) {
      return (o.equals("+") || o.equals("-") || o.equals("*") || o.equals("/") || o.equalsIgnoreCase("swap") || o.equalsIgnoreCase("rot"));
   }

   static boolean operatorIsSwap(String o) {
      return (o.equalsIgnoreCase("swap"));
   }

   static boolean operatorIsRot(String o) {
      return (o.equalsIgnoreCase("rot"));
   }

   static int getStackSize(Stack s) {
      int nodeCount = 0;
      for (Object ignored : s) {
         nodeCount++;
      }
      return nodeCount;
   }

   public static boolean isNumeric(String str) {
      try {
         Double.parseDouble(str);
         return true;
      } catch(NumberFormatException e){
         return false;
      }
   }



   public static void main (String[] param) {
      String rpn = "2 5 SWAP -";
      System.out.println ("RPN: " + rpn);
      Tnode res = buildFromRPN (rpn);
      System.out.println ("Tree: " + res);
   }
}

